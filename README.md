# APPLICATION SECURITY PIPELINE
*This project is meant as a basic guide to utitlizing the built-in application security tools available in the continuous integration/continuous deployment (CI/CD) GitLab pipeline when commiting code to a repository. We will explore the significance of each tool, as well as demonstrate their abilities below.*

**Currently, this project only focuses on the tools available in GitLab's free tier.* 

<br>

## **INTRODUCTION**
Using GitLab's built-in application security tools is critical to ensure the security and quality of your application. GitLab's security tools automate the process of detecting and remedying security vulnerabilities, ensuring compliance with industry standards and regulations, reducing manual effort, and integrating security into your development process. By using GitLab's security tools, you can improve developer productivity, maintain code quality, and avoid costly security breaches that can damage your organization's reputation. Overall, GitLab's built-in application security tools provide a comprehensive solution for improving the security and quality of your application.

<br>

**GitLab Free Tier Security Tools:**
- Static Application Security Testing (SAST)
- Infrastructure as Code (IaC) Scanning
- Container Scanning
- Secret Detection

**GitLab Ultimate Security Tools:**
- Dynamic Application Security Testing (DAST)
- Dependency Scanning
- API Fuzzing
- Coverage Fuzzing

<br>

## **PREREQUISITES**

In order to follow along with this guide, you will need your own GitLab account and a basic understanding of git. This repository has been designed to use simple code that is easy to understand for beginning developers. If you do not have your own project to apply this information, you can use the code found in this repo. All coding will be layed out in a step-by-step manner for each of the security tool demonstrations found in this guide. 

## **SAST**
GitLab SAST is a security feature designed to help developers find and fix security vulnerabilities in their source code before it gets deployed to production. SAST analyzes the codebase and checks for known security flaws, such as SQL injection, cross-site scripting, and buffer overflows, among others. SAST can scan various programming languages, including Ruby, JavaScript, Python, and Java, among others. SAST scans provide a detailed report of the vulnerabilities detected, including their severity level and location in the codebase. This enables developers to prioritize their fixes and quickly address critical security issues.

<br>

**In the Free and Premium tiers, users can:** 
- automatically scan code with appropriate analyzers
- configure SAST scanners
- customize SAST settings
- download JSON reports

**In the Ultimate tier, users can also:** 
- see new findings in the merge request widget
- manage vulnerabilities
- access the Security Dashboard
- configure SAST in the UI
- customize SAST rulesets
- detect false positives
- track moved vulnerabilities

<br>

For more information, please visit the [GitLab SAST Documentation](https://docs.gitlab.com/ee/user/application_security/sast/). 

<br>

[Click here to see our SAST demonstration.](./SAST/README.md)
















<br><br><br>

## **ROADMAP**

- [ ] SAST
- [ ] Secret Detection
- [ ] IaC Scanning
- [ ] Container Scanning


<br><br>

*Last updated on March 10, 2023 by Samantha Hipple*
