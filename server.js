'use strict';
var http = require('http');
var fs = require('fs');
var port = process.env.PORT || 8080;

http.createServer(function (req, res) {
    if (req.url === '/') {
        fs.readFile('index.html', function(err, data) {
            if (err) { 
                res.writeHead(500);
                res.end("Error loading index.html");
            } else {
                res.writeHead(200, {'Content-Type': 'text/html'});
                res.end(data);
            }
        });
    } else {
        res.writeHead(404);
        res.end("Page not found.");
    }
}).listen(port);