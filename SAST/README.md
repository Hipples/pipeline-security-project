# **SAST DEMO**

## **STEP 1: Enable SAST**

![check enable SAST](./images/enable_sast_at_creation.png) 

*When creating a new project/repository, GitLab provides an option to enable SAST before you even press ```Create project```.*

<br>

## **STEP 2: Create New Project**

![new project repository](./images/new_project.png)

*With SAST enabled, your new project repository will start out with two files: `README.md` and `.gitlab-ci.yml`.*

The `.gitlab-ci.yml` file is the configuration file used by GitLab CI/CD to define the stages and jobs that make up a CI/CD pipeline. The YAML file is responsible for defining the tasks that should be executed for each stage of the pipeline and specifying the conditions under which they should run. This can include tasks like building and testing the code, deploying it to a staging environment, running security scans or performance tests, and deploying it to production. Additionally, this file is used to define variables, artifacts, and dependencies that are needed for each job to run successfully. 

Once the `.gitlab-ci.yml` file is defined, GitLab will automatically build, test, and deploy your code based on the pipeline stages and jobs specified. 